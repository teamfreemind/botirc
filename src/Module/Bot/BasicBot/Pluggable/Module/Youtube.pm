package Bot::BasicBot::Pluggable::Module::Youtube;
use base qw(Bot::BasicBot::Pluggable::Module);

use 5.014;
use warnings;

use Mojo::UserAgent;
use JSON qw (decode_json);
use LWP::Simple;

use constant URL_YOUTUBE => 'https://youtube.com/watch?v=';
use constant API_KEY => 'AIzaSyAO9FuNU0ULUUWFoVIYaRc3GNLaEV25J7c';
# Create your own API_KEY at:
# https://developers.google.com/youtube/v3/getting-started

use constant API_URL => 'https://www.googleapis.com/youtube/v3/search?part=snippet&key=';
use constant API_OPTIONS => '&type=video&q=';

sub told {
    my ( $self, $mess ) = @_;
    my $body = $mess->{body};

    my ( $command, $param ) = split( /\s+/, $body, 2 );
    $command = lc($command);

    return find($self, $mess) if ( $command eq "!yt" );
}

sub find
{
  my ($self, $msg) = @_;
  my $string = "";
  my @array = split(" ", $msg->{body});
  # Cut the string into an array
  
  foreach my $elt (@array)
  {
    next if ($elt =~ /!yt/);
    $string .= $elt . "%22";
    # %22 is used in an URL in order to replace space
    # i.e "Hello World" => "Hello%22World"
  }
  $string = substr($string, 0, -3);  # Cut the !yt
  my $browser = LWP::UserAgent->new;
  chomp $string;
  my $url = API_URL . API_KEY . API_OPTIONS . $string;
  my $json = $browser->get ( $url );
  my $decoded_json = decode_json ( $json->content );
  my $record = $decoded_json->{items}[0];
  my $id = $record->{id}{videoId};
  my $title = $record->{snippet}{title};
  $self->say(
    who=>$msg->{who},
    channel=>$msg->{channel},
    address=>$msg->{who},
    body=>$title . "  " . URL_YOUTUBE . "$id",
  );
}
1;
