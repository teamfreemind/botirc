package Bot::BasicBot::Pluggable::Module::Greplog;
use base qw(Bot::BasicBot::Pluggable::Module);
use warnings;
use strict;

use DateTime;
use File::Spec::Functions qw(curdir);

sub help {
    return
"Grep log";
}

sub told {
    my ( $self, $mess ) = @_;
    my $body = $mess->{body};
    my ( $command, $param ) = split( /\s+/, $body, 2 );
    $command = lc($command);

    if ( $command =~ "!greplog" ) {
        handleGrepLog($self, $mess);
    }
}

sub handleGrepLog
{
   my ( $self, $mess ) = @_;
   return unless checkAdmin($mess->{who});
   my $channel = $mess->{channel};
   my $body = $mess->{body};
   my @words = split / /, $body;
   my $size = @words;
   return "Mauvaise syntaxe : !greplog #chan JJ.MM.ANNE HH:MM(-HH:MM)" if $size != 4;
   return "Mauvaise date : ". $words[2] if ($words[2] !~ /^[0-9][0-9]\.[0-9][0-9]\.[0-9][0-9][0-9][0-9]$/);
   my ($day, $month, $year) = (split /\./, $words[2]);
   my $rc = checkDate($day, $month, $year);
   return "La date n'existe pas" if ($rc);
   $rc = checkDateFutur($day, $month, $year);
   return "Hey, tu te crois dans Retour vers le Futur ou quoi !? DATE INVALIDE BOULET ! " if ($rc);
   $rc = checkDirectory($day, $month, $year, $channel);
   return "Le fichier n'existe pas :(" if ($rc);
   readLog($day, $month, $year, $channel, $words[3], $self, $mess);
}

sub checkAdmin
{
    my $who = shift;
    my @array = qw(Neoki Alibaba Orkido kortex kortex[Away] NeoWay);
    if ( grep( /^$who$/, @array ) ) {
        return 1;
    }
    return 0;

}

sub checkDate
{
    my ($day, $month, $year) = @_;
    my $dt; 
    eval { $dt = DateTime->new( 
        year => $year, 
        month => $month, 
        day => $day);
    }; 
    return 1 if $@;
}

sub checkDateFutur
{
    my ($checkDay, $checkMonth, $checkYear) = @_;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    $year+= 1900;
    $mon++;
    return 1 if ($year < $checkYear);
    return 1 if ($mon < $checkMonth);
    return 1 if ($mday < $checkDay);
    return 0;
}

sub checkDirectory
{
    my ($checkDay, $checkMonth, $checkYear, $channel) = @_;
    my $date = $checkYear . $checkMonth . $checkDay;
    $channel =~ s/^#//;
    my $file = $channel . "_" . $date . ".log";
    return 1 unless (-e curdir() . "/Log/" . uc($channel) . "/" . $file);
    return 0;
}

sub readLog
{
    my ($checkDay, $checkMonth, $checkYear, $channel, $conditionDate, $self, $msg) = @_;
    my $date1 = undef;
    my $date2 = undef;
    if ($channel =~ /-/)
    {
        ($date1, $date2) = (split /-/, $conditionDate);
    }
    else
    {
        $date1 = $conditionDate;
    }
    my $date = $checkYear . $checkMonth . $checkDay;
    $channel =~ s/^#//;
    my $file = $channel . "_" . $date . ".log";
    my $log = curdir() . "/Log/" . uc($channel) . "/" . $file;
    open (my $fh, "<", $log);
    my @array = <$fh>;
    close $fh;
    my @newArray;
    foreach (@array)
    {
        push @newArray, $_ if ($_ =~ /^\[#$channel $date1/);
        if (defined($date2))
        {
            push @newArray, $_ if ($_ =~ /^\[#$channel $date2/);
        }
    }
    push @newArray, "---------------------------------------------------------";
    $self->say(
    who=>$msg->{who},
    channel=>"msg",
    address=>"msg",
    body=>"@newArray\n",
  );
}

1;

